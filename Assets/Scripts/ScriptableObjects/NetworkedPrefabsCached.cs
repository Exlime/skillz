﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkedPrefabsCached : ScriptableObject
{
    public List<NetworkedPrefab> networkedPrefabs = new List<NetworkedPrefab>();
}
