﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MakeNetworkedPrefabsCachedAsset
{
#if UNITY_EDITOR
    [MenuItem("Assets/Create/NetworkedPrefabsCached")]
    public static void CreateMyAsset()
    {
        NetworkedPrefabsCached asset = ScriptableObject.CreateInstance<NetworkedPrefabsCached>();

        AssetDatabase.CreateAsset(asset, "Assets/NewScripableObject.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
#endif
}
