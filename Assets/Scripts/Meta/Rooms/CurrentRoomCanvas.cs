﻿using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CurrentRoomCanvas : MonoBehaviour
{
    public PlayerListingsMenu playerListingsMenu;
    public LeaveRoomMenu leaveRoomMenu;

    private RoomsCanvases roomsCanvases;

    public void FirstInitiliaze(RoomsCanvases canvases)
    {
        roomsCanvases = canvases;
        playerListingsMenu.FirstInitialize(canvases);
        leaveRoomMenu.FirstInitialize(canvases);
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
