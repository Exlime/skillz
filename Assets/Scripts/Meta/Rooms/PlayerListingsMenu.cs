﻿using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerListingsMenu : MonoBehaviourPunCallbacks
{
    [SerializeField] private Transform content;
    [SerializeField] private PlayerListing playerListing;
    [SerializeField] private Text startGameText;
    

    private List<PlayerListing> playerListings = new List<PlayerListing>();

    private RoomsCanvases roomsCanvases;
    //private bool isReady = false;

    public void FirstInitialize(RoomsCanvases canvases)
    {
        roomsCanvases = canvases;
    }

    public override void OnEnable()
    {
        base.OnEnable();
        GetCurrentRoomPlayers();

        //isReady = false;
        //startGameText.text = "Press to be Ready";
        if (PhotonNetwork.IsMasterClient)
        {
            startGameText.text = "Start Game";
            startGameText.fontSize = 20;
            //playerListings[playerListings.FindIndex(x => x.Player == PhotonNetwork.LocalPlayer)].Ready = true; // Master is always ready
        }
        else
        {
            startGameText.text = "Only room creator can start the Game";
            startGameText.fontSize = 13;
        }
    }

    public override void OnDisable()
    {
        base.OnDisable();
        for (int i = 0; i < playerListings.Count; i++)
        {
            Destroy(playerListings[i].gameObject);
        }

        playerListings.Clear();
    }

    private void GetCurrentRoomPlayers()
    {
        if (!PhotonNetwork.IsConnected || PhotonNetwork.CurrentRoom == null || PhotonNetwork.CurrentRoom.Players == null)
        {
            return;
        }

        foreach(var playerInfo in PhotonNetwork.CurrentRoom.Players)
        {
            AddPlayerListing(playerInfo.Value);
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        AddPlayerListing(newPlayer);
    }

    private void AddPlayerListing(Player newPlayer)
    {
        int index = playerListings.FindIndex(x => x.Player == newPlayer);

        if (index != -1)
        {
            playerListings[index].SetPlayerInfo(newPlayer);
        }
        else
        {
            PlayerListing listing = Instantiate(playerListing, content);
            if (listing != null)
            {
                listing.SetPlayerInfo(newPlayer);
                playerListings.Add(listing);
            }
        }
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        int index = playerListings.FindIndex(x => x.Player == otherPlayer);
        if (index != -1)
        {
            Destroy(playerListings[index].gameObject);
            playerListings.RemoveAt(index);
        }
    }

    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        roomsCanvases.CurrentRoomCanvas.leaveRoomMenu.OnClick_LeaveRoom();
    }

    public void OnClick_StartGame()
    {
        if(PhotonNetwork.IsMasterClient)
        {   
            //for (int i = 0; i < playerListings.Count; i++)
            //{
            //    if(!playerListings[i].Ready)
            //    {
            //        Debug.LogWarning("Not all players are ready");
            //        return;
            //    }
            //}
            PhotonNetwork.CurrentRoom.IsOpen = false;
            PhotonNetwork.CurrentRoom.IsVisible = false;
            PhotonNetwork.LoadLevel(1);
        }
        //else
        //{
        //    isReady = !isReady;
        //    startGameText.text = isReady ? "You're Ready" : "Press to be Ready";
        //    base.photonView.RPC("RPC_ChangeReadyState", RpcTarget.MasterClient, PhotonNetwork.LocalPlayer, isReady);
        //}
    }

    //[PunRPC]
    //private void RPC_ChangeReadyState(Player player, bool ready)
    //{
    //    int index = playerListings.FindIndex(x => x.Player == player);
    //    if (index != -1)
    //    {
    //        playerListings[index].Ready = ready;
    //    }
    //}
}
