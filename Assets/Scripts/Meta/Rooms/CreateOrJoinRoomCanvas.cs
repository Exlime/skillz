﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateOrJoinRoomCanvas : MonoBehaviour
{
    [SerializeField] private CreateRoomMenu createRoomMenu;
    [SerializeField] private RoomListingsMenu roomListingsMenu;

    private RoomsCanvases roomsCanvases;

    public void FirstInitiliaze(RoomsCanvases canvases)
    {
        roomsCanvases = canvases;
        createRoomMenu.FirstInitiliaze(canvases);
        roomListingsMenu.FirstInitialize(canvases);
    }
}
