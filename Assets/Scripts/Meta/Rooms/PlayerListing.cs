﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerListing : MonoBehaviour
{
    [SerializeField] private Text text;

    public Player Player { get; private set; }
    //public bool Ready = false;

    public void SetPlayerInfo(Player player)
    {
        Player = player;
        //int result = (int) player.CustomProperties["RandomNumber"];
        text.text = player.NickName;
    }
}
