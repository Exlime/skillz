﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RoomsCanvases : MonoBehaviour
{
    public TextMeshProUGUI notificationsText;
    [SerializeField] private CreateOrJoinRoomCanvas createOrJoinRoomCanvas;
    [SerializeField] private CurrentRoomCanvas currentRoomCanvas;
    [SerializeField] private NicknameChangeMenu nicknameChangeCanvas;

    public CreateOrJoinRoomCanvas CreateOrJoinRoomCanvas { get => createOrJoinRoomCanvas; set => createOrJoinRoomCanvas = value; }
    public CurrentRoomCanvas CurrentRoomCanvas { get => currentRoomCanvas; set => currentRoomCanvas = value; }

    private void Awake()
    {
        FirstInitialize();    
    }

    private void FirstInitialize()
    {
        createOrJoinRoomCanvas.FirstInitiliaze(this);
        currentRoomCanvas.FirstInitiliaze(this);
        nicknameChangeCanvas.FirstInitiliaze(this);
    }
}
