﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NicknameChangeMenu : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI nickname;
    [SerializeField] private Button applyNicknameChangeBtn;
    [SerializeField] private GameObject nicknameChangedNotification;

    private RoomsCanvases roomsCanvases;

    public void FirstInitiliaze(RoomsCanvases canvases)
    {
        roomsCanvases = canvases;
    }

    public void OnClick_Change()
    {
        if (!PhotonNetwork.IsConnected)
        {
            roomsCanvases.notificationsText.gameObject.SetActive(true);
            MethodInvoker.InvokeAfterDelay(this, () => roomsCanvases.notificationsText.gameObject.SetActive(false), 1.5f);
            return;
        }

        PhotonNetwork.LocalPlayer.NickName = nickname.text;
        nicknameChangedNotification.SetActive(true);
        MethodInvoker.InvokeAfterDelay(this, () => nicknameChangedNotification.SetActive(false), 1.0f);

    }
}
