﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestConnect : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log("Connecting...");
        PhotonNetwork.SendRate = 20; // 20 by default
        PhotonNetwork.SerializationRate = 15; // 10 by default
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.GameVersion = "0.0.1";
        PhotonNetwork.ConnectUsingSettings();
        PhotonNetwork.NickName = GetRandomNickname();
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to server");
        //Debug.Log("Nickname - " + PhotonNetwork.LocalPlayer.NickName);

        if(!PhotonNetwork.InLobby)
        {
            PhotonNetwork.JoinLobby();
        }
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("Disconnected from server for reason : " + cause.ToString());
    }

    private string GetRandomNickname()
    {
        int value = Random.Range(0, 99);
        return "Player " + value;
    }
}
