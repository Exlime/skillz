﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MethodInvoker
{
    public static void InvokeAfterDelay(MonoBehaviour monoBehOwner, Action method, float time)
    {
        monoBehOwner.StartCoroutine(InvokeAfterDelay_IE(method, time));
    }

    private static IEnumerator InvokeAfterDelay_IE(Action method, float time)
    {
        yield return new WaitForSeconds(time);
        method();

    }
}
