﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public static class ExtTransforms
{
    public static void DestroyChildren(this Transform t, bool destroyImmediately = false)
    {
        foreach(Transform child in t)
        {
            if(destroyImmediately)
            {
                MonoBehaviour.DestroyImmediate(child.gameObject); 
            }
            else
            {
                MonoBehaviour.Destroy(child.gameObject);
            }
        }
    }
}
