﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class FaceCamera : MonoBehaviour
{
    private Transform localCameraObjectTransform;

    private void Awake()
    {
        localCameraObjectTransform = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Transform>();
    }
    private void Update()
    {
        transform.LookAt(localCameraObjectTransform);
        //transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles - new Vector3(0.0f, transform.rotation.eulerAngles.y, 0.0f));
    }
}
