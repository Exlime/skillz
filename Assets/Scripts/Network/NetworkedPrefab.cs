﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NetworkedPrefab
{
    public GameObject Prefab;
    public string Path;

    public NetworkedPrefab(GameObject obj, string path)
    {
        Prefab = obj;
        Path = ReturnPrefabPathModified(path);
        //Received : Assets/Resources/File.prefab
        //Modified : Resources/File
    }

    private string ReturnPrefabPathModified(string path)
    {
        path = path.Replace("Assets/Resources/", "").Replace(".prefab", "");
        return path;
    }
}
