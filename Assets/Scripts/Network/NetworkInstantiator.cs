﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public static class NetworkInstantiator 
{
    [SerializeField] private static NetworkedPrefabsCached networkedPrefabsCache;

    public static GameObject NetworkInstantiate(GameObject obj, Vector3 position, Quaternion rotation)
    {
        //Debug.Log(">>> networkedPrefabs count : " + networkedPrefabsCache.networkedPrefabs.Count);
        foreach (var networkedPrefab in networkedPrefabsCache.networkedPrefabs)
        {
            //Debug.Log(">>> Networked Prefab found : " + networkedPrefab.Prefab.name + " >>> Path : " + networkedPrefab.Path);
            if(networkedPrefab.Prefab == obj)
            {
                //Debug.Log("");
                if(networkedPrefab.Path != string.Empty)
                {
                    GameObject result = PhotonNetwork.Instantiate(networkedPrefab.Path, position, rotation);
                    //Debug.Log(">>> PhNetwork Instantiate result name : " + result.name + "<<<");
                    return result;
                }
                else
                {
                    Debug.LogError("Path is empty for gameobject : " + networkedPrefab.Prefab.name);
                    return null;
                }
                
            }
        }
        Debug.Log("NO NETWORKED PREFABS");
        return null;
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    public static void PopulateNetworkedPrefabs()
    {
        networkedPrefabsCache = Resources.Load<NetworkedPrefabsCached>("NetworkedPrefabsCached");
#if UNITY_EDITOR
        networkedPrefabsCache.networkedPrefabs.Clear();

        GameObject[] results = Resources.LoadAll<GameObject>("");
        for (int i = 0; i < results.Length; i++)
        {
            if(results[i].GetComponent<PhotonView>() != null)
            {
                string path = AssetDatabase.GetAssetPath(results[i]);
                networkedPrefabsCache.networkedPrefabs.Add(new NetworkedPrefab(results[i], path));
            }
        }
#endif
    }
}
