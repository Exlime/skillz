﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelScenarioSkew : MonoBehaviour
{
    [SerializeField] private float scenarioTime = 30.0f;
    //public float smooth = 1f;
    //private Quaternion targetRotation;
    //private bool gonnaRotate = false;

    //void Start()
    //{
    //    targetRotation = transform.rotation;
    //}

    //void Update()
    //{
    //    if (gonnaRotate)
    //    {
    //        targetRotation *= Quaternion.AngleAxis(60, Vector3.up);
    //    }
    //    transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, 10 * smooth * Time.deltaTime);

    //}

    private void Awake()
    {
        MethodInvoker.InvokeAfterDelay(this, SkewLevel, scenarioTime);
    }

    public void SkewLevel()
    {
        gameObject.transform.DORotate(new Vector3(-58f, 0.0f, 0.0f), 4.0f);
    }
}
