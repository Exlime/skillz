﻿using Photon.Pun;
using System;
using UnityEngine;

public class ExplosionSkill : MonoBehaviourPunCallbacks
{
    public LayerMask destructibleMask;
    public LayerMask obstacleMask;
    public float explosionForce;

    [SerializeField] private GameObject explosionObject;
    [SerializeField] private float skillSize = 3.0f;
    [SerializeField] private Transform skillDetonationPoint;
    [SerializeField] private Rigidbody characterRigidbody;
    [SerializeField] private PhotonView characterPhotonView;
    [SerializeField] private float cooldownDuration = 1.0f;
    [SerializeField] private bool isDummy = false;
    [SerializeField] private float boomPrepairingTime = 0.8f;

    private MobileInputLinks mobileInputLinks;

    private bool onCooldown = false;
    bool gonnaCastSkill = false;

    private void Awake()
    {
        if(Application.isMobilePlatform) // default
        //if (!Application.isMobilePlatform) //debug
        {
            mobileInputLinks = GameObject.FindGameObjectWithTag("MobileInput").GetComponent<MobileInputLinks>();
            mobileInputLinks.Skill1Btn.onClick.AddListener(() =>
            {
                gonnaCastSkill = true;
                MethodInvoker.InvokeAfterDelay(this, () => gonnaCastSkill = false, 0.2f);
            });
        }
    }

    public void Update()
    {
        if(!onCooldown && !isDummy)
        {
            if (characterPhotonView.IsMine)
            {

                if(!Application.isMobilePlatform) // default
                //if (Application.isMobilePlatform) // debug
                {
                    gonnaCastSkill = Input.GetButtonDown("Fire1");
                }

                if (gonnaCastSkill)
                {
                    MakeBoom();
                    Cooldown();
                }
            }
        }
    }

    private void MakeBoom()
    {
        ActivateSkill();
        var skillDetonationPointPosition = skillDetonationPoint.position;
        MethodInvoker.InvokeAfterDelay(this, () => { MakePhysicsBoom(skillDetonationPointPosition); } , boomPrepairingTime);
    }

    private void MakePhysicsBoom(Vector3 skillDetonationPointPosition)
    {
        Collider[] collidersNear = Physics.OverlapSphere(skillDetonationPointPosition, skillSize, destructibleMask);
        foreach (Collider collider in collidersNear)
        {
            Transform target = collider.transform;
            Rigidbody targetRigidbody = target.GetComponent<Rigidbody>();
            if (targetRigidbody != null)
            {
                var targetCharacterExplosionSkillPhotonView = targetRigidbody.GetComponent<CharacterLinks>()
                    .ExplosionSkillPhotonView;

                targetCharacterExplosionSkillPhotonView
                    .RPC("RPC_MakeBoomForCharacterOwner", RpcTarget.All, explosionForce,
                    skillDetonationPointPosition, skillSize);
            }
        }
    }

    private void ActivateSkill()
    {
        GameObject skillDetonation = NetworkInstantiator.NetworkInstantiate(explosionObject, skillDetonationPoint.position, Quaternion.identity);
        skillDetonation.transform.position = skillDetonationPoint.position;
        MethodInvoker.InvokeAfterDelay(this, () => { PhotonNetwork.Destroy(skillDetonation); }, 2f);

        MethodInvoker.InvokeAfterDelay(this, DealDamage, boomPrepairingTime);

    }

    private void DealDamage()
    {
        // Damage Causing
        Collider[] trappedPlayers = Physics.OverlapSphere(skillDetonationPoint.position, skillSize, LayerMask.GetMask("Players"));
        if (trappedPlayers.Length > 0)
        {
            foreach (var player in trappedPlayers)
            {
                player.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", player.GetComponent<PhotonView>().Owner, 15.0f);
            }
        }
    }

    [PunRPC]
    public void RPC_MakeBoomForCharacterOwner(float explosionForce, Vector3 explosionPosition, float explosionRadius)
    {
        characterRigidbody.AddExplosionForce(explosionForce, explosionPosition, explosionRadius, 1, ForceMode.Impulse);
    }

    private void Cooldown()
    {
        onCooldown = true;
        MethodInvoker.InvokeAfterDelay(this, () => { onCooldown = false; }, cooldownDuration);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(255, 0, 0, 0.1f);
        Gizmos.DrawSphere(skillDetonationPoint.position, skillSize);
    }
}