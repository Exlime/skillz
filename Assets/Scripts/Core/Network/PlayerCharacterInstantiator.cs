﻿using Cinemachine;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacterInstantiator : MonoBehaviourPunCallbacks
{
    public GameObject prefab;
    public static List<PlayerCharacter> playerCharacters = new List<PlayerCharacter>();
    
    [SerializeField] private CinemachineVirtualCamera cinemachineCamera;
    //public static ExitGames.Client.Photon.Hashtable playerCharactersProperty = new ExitGames.Client.Photon.Hashtable();


    private void Awake()
    {
        if(PhotonNetwork.IsMasterClient)
        {
            Vector3 instantiateOffset = new Vector3(1.5f, 0.0f, 0.0f);
            foreach (var player in PhotonNetwork.PlayerList)
            {
                GameObject characterGO = NetworkInstantiator.NetworkInstantiate(prefab, transform.position + instantiateOffset,
                    Quaternion.identity);
                characterGO.name = characterGO.name + " - " + player.NickName;

                characterGO.GetComponent<PhotonView>().TransferOwnership(player);
                var view = characterGO.GetComponent<PhotonView>();
                instantiateOffset += new Vector3(1.5f, 0.0f, 0.0f);
                var playerCharacter = new PlayerCharacter(characterGO, player);
                //playerCharactersProperty.Add(player.NickName, playerCharacter);
                playerCharacters.Add(playerCharacter);
                //Debug.Log("CharacterInstantiator Character view Owner == " + view.Owner.NickName + " for character : " + characterGO.name +
                    //" and for player : " + player.NickName);
            }
            StartCoroutine(SetCameraWithDelay());
        }

        //if (!PhotonNetwork.IsMasterClient)
        //{
        //    Debug.Log("Characters Property count : " + playerCharacters.Count);
        //    foreach (var playerCharacter in playerCharacters)
        //    {
        //        //Debug.Log("Player Key Nickname: " + playerCharacter.Key);
        //        //Debug.Log("Player Value Nickname: " + ((PlayerCharacter)playerCharacter.Value).playerRef.NickName);
        //    }
        //}
    }

    private IEnumerator SetCameraWithDelay()
    {
        yield return new WaitForSeconds(0.8f);
        base.photonView.RPC("RPC_SetCameraToLocalPlayer", RpcTarget.Others);
        RPC_SetCameraToLocalPlayer();
        yield return null;
    }

    [PunRPC]
    public void RPC_SetCameraToLocalPlayer()
    {
        GameObject[] playerChars = GameObject.FindGameObjectsWithTag("Player");

        GameObject localPlayerChar = null;


        foreach (var playerChar in playerChars)
        {
            if (playerChar.GetComponent<PhotonView>().IsMine)
            {
                //Debug.Log("LocalPlayerFinder isMine? == " + playerChar.GetComponent<PhotonView>().IsMine + " for " + playerChar.name);
                localPlayerChar = playerChar;
                //Debug.Log("Found player char : " + playerChar.name);
                //Debug.Log("Local player nickname : " + PhotonNetwork.LocalPlayer.NickName); // why masterClient owns both players photon views?
            }
        }

        cinemachineCamera.LookAt = localPlayerChar.transform;
        cinemachineCamera.Follow = localPlayerChar.transform;
    }
}

public class PlayerCharacter
{
    public GameObject characterGO;
    public Player playerRef;

    public PlayerCharacter(GameObject characterGO, Player playerRef)
    {
        this.characterGO = characterGO;
        this.playerRef = playerRef;
    }
}

