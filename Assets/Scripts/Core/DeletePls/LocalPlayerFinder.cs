﻿using Cinemachine;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class LocalPlayerFinder : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera cinemachineCamera;

    private void Awake()
    {
        cinemachineCamera = GetComponent<CinemachineVirtualCamera>();
    }

    public IEnumerator DebugLogOwnerships()
    {
        yield return new WaitForSeconds(3.0f);
        GameObject[] playerChars = GameObject.FindGameObjectsWithTag("Player");
        foreach (var playerChar in playerChars)
        {
            if (playerChar.GetComponent<PhotonView>().IsMine)
            {
                Debug.LogError("LocalPlayerFinder isMine? == " + playerChar.GetComponent<PhotonView>().IsMine + " for " + playerChar.name);
                //Debug.Log("Found player char : " + playerChar.name);
                Debug.LogError("Local player nickname : " + PhotonNetwork.LocalPlayer.NickName); // why masterClient owns both players photon views?
            }
        }

        yield return null;
    }
}
