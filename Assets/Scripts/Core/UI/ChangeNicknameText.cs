﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ChangeNicknameText : MonoBehaviour, IPunObservable
{
    [SerializeField] private TextMeshProUGUI nicknameText;
    [SerializeField] private PhotonView characterPhotonView;

    private void Awake()
    {
        MethodInvoker.InvokeAfterDelay(this, () => characterPhotonView.RPC("AssignNickText", RpcTarget.All), 1.0f);
    }

    [PunRPC]
    public void AssignNickText()
    {
        if (characterPhotonView.IsMine)
        {
            nicknameText.text = PhotonNetwork.LocalPlayer.NickName;
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.IsWriting)
        {
            stream.SendNext(nicknameText.text);
        }
        else if(stream.IsReading)
        {
            nicknameText.text = (string)stream.ReceiveNext();
        }
    }
}
