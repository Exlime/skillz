﻿using MenteBacata.ScivoloCharacterController.Internal;
using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class MovementController : MonoBehaviourPun
{

    public bool isDummy = false;

    [Header("Input")]
    private MobileInputLinks mobileInputLinks;

    [Header("Movement")]
    [SerializeField] private Rigidbody rb;
    //[SerializeField] private CharacterController controller;
    [SerializeField] private float speed = 10.0f;
    // Start is called before the first frame update
    [SerializeField] private float jumpForce = 10.0f;
    [SerializeField] private Vector3 jumpCheckDistance;
    [SerializeField] private Transform groundCheckTransform;
    [SerializeField] private LayerMask groundCheckLayerMask;
    private bool isGrounded = true;
    private bool blockGroundCheck = false;
    private bool gonnaJump = false;

    [Header("Animation")]
    [SerializeField] Animator animatorController;
    [SerializeField] Transform modelTransform;


    private void Awake()
    {
        if (Application.isMobilePlatform) // default
        //if (!Application.isMobilePlatform) // debug
        {
            mobileInputLinks = GameObject.FindGameObjectWithTag("MobileInput").GetComponent<MobileInputLinks>();
            mobileInputLinks.jumpBtn.onClick.AddListener(() =>
            {
                if(isGrounded)
                {
                    gonnaJump = true;
                    MethodInvoker.InvokeAfterDelay(this, () => gonnaJump = false, 0.5f);
                }
            });
        }
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (base.photonView.IsMine)
        {
            List<Collider> grounds = Physics.OverlapBox(groundCheckTransform.position, jumpCheckDistance, 
                Quaternion.identity, groundCheckLayerMask).ToList();


            int removeLocalPlayerCharacterFromGroundsIndex = -1;
            for (int i = 0; i < grounds.Count; i++)
            {
                if(grounds[i].gameObject == gameObject)
                {
                    removeLocalPlayerCharacterFromGroundsIndex = i;
                }
            }
            if (removeLocalPlayerCharacterFromGroundsIndex > -1)
            {
                grounds.Remove(grounds[removeLocalPlayerCharacterFromGroundsIndex]);
            }


            if (grounds.Count > 0 && !blockGroundCheck)
            {
                isGrounded = true;
            }


            float horizontal;
            float vertical;

            if (Application.isMobilePlatform) // default
            //if (!Application.isMobilePlatform) // debug
            {
                horizontal = mobileInputLinks.joystick.Horizontal;
                vertical = mobileInputLinks.joystick.Vertical;
            }
            else
            {
                horizontal = Input.GetAxisRaw("Horizontal");
                vertical = Input.GetAxisRaw("Vertical");
                gonnaJump = Input.GetButtonDown("Jump");
            }
            Vector3 newDirection = new Vector3(horizontal, 0.0f, vertical).normalized;
            if (Mathf.Abs(newDirection.x) < 0.1f && Mathf.Abs(newDirection.z) < 0.1f)
            {
                animatorController.SetTrigger("Idle");
            }

            if (Mathf.Abs(newDirection.x) > 0.1f || Mathf.Abs(newDirection.z) > 0.1f)
            {
                animatorController.SetTrigger("Run");
            }
            transform.Translate(newDirection * speed * Time.deltaTime);

            
            //bool jump = Input.GetButtonDown("Jump");
            if (gonnaJump && isGrounded)
            {
                rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
                isGrounded = false;
                blockGroundCheck = true;
                MethodInvoker.InvokeAfterDelay(this, () => blockGroundCheck = false, 0.4f);
            }

            modelTransform.LookAt(modelTransform.position + newDirection);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(groundCheckTransform.position, jumpCheckDistance);
    }

    
}
