﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPSystem : MonoBehaviour, IPunObservable
{
    [SerializeField] private float maxHP = 100.0f;
    [SerializeField] private float currentHP;
    [SerializeField] private Image currentHPImage;
    private GameObject gameOverCanvas;

    private void Awake()
    {
        gameOverCanvas = GameObject.FindGameObjectWithTag("GameOverCanvas");
        currentHP = maxHP;
    }

    [PunRPC]
    public void TakeDamage(float damage)
    {
        currentHP -= damage;
        currentHPImage.fillAmount = currentHP / maxHP;
        if(currentHP <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        //gameOverCanvas.SetActive(true);
        gameOverCanvas = Resources.Load<GameObject>("Widgets/GameOverCanvas");
        Instantiate(gameOverCanvas);
        PhotonNetwork.Destroy(gameObject);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(currentHP);
        }
        else if (stream.IsReading)
        {
            currentHP = (float)stream.ReceiveNext();
            currentHPImage.fillAmount = currentHP / maxHP;
        }
    }
}
