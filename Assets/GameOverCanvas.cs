﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverCanvas : MonoBehaviour
{
    public void OnClick_BackToMenu()
    {
        Application.Quit();
    }
}
