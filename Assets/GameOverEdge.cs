﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class GameOverEdge : MonoBehaviour
{
    [SerializeField] private Vector3 size;
    [SerializeField] private GameObject gameOverCanvas;
    [SerializeField] private LayerMask playerLayer;

    private void Update()
    {
        var catchedObjects = Physics.OverlapBox(transform.position, size, Quaternion.identity, playerLayer);
        if (catchedObjects != null)
        {
            foreach(var catchedObject in catchedObjects)
            {
                if(catchedObject.gameObject.GetComponent<PhotonView>().IsMine)
                {
                    if(catchedObject.gameObject.GetComponent<MovementController>().isDummy == false)
                    {
                        gameOverCanvas.SetActive(true);
                    }
                }
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireCube(transform.position, size);
    }
}
