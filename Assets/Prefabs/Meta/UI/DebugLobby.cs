﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugLobby : MonoBehaviour
{
    public void PrintLobbyName()
    {
        Debug.Log(PhotonNetwork.CurrentLobby.Name);
    }
}
